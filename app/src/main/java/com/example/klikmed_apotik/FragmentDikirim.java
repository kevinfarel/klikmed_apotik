package com.example.klikmed_apotik;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.klikmed_apotik.Adapter.Adapter_siapdikirim;
import com.example.klikmed_apotik.model.FinishOrder;
import com.example.klikmed_apotik.retrofit.API;
import com.example.klikmed_apotik.retrofit.Client;
import com.example.klikmed_apotik.retrofit.Response.BaseResponse;
import com.example.klikmed_apotik.retrofit.Response.LoadDikirimResponse;
import com.example.klikmed_apotik.session.SessionManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link FragmentDikirim.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link FragmentDikirim#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FragmentDikirim extends Fragment implements Adapter_siapdikirim.AdapterCallback {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public FragmentDikirim() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment FragmentDikirim.
     */
    // TODO: Rename and change types and number of parameters
    public static FragmentDikirim newInstance(String param1, String param2) {
        FragmentDikirim fragment = new FragmentDikirim();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_dikirim, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initComponent(view);
        loadWaitingOrder(new SessionManager(getActivity()).getAccountApotik().getPharmacy_id());
    }
    RecyclerView recyclerView_dikirim;
    SwipeRefreshLayout swipeRefreshLayout;
    Adapter_siapdikirim adapter_siapdikirim;
    LinearLayout empty;
    ArrayList<FinishOrder> finishOrders = new ArrayList<>();
    private void initComponent(View view) {
        recyclerView_dikirim = view.findViewById(R.id.recyclerview_dikirim);
        swipeRefreshLayout = view.findViewById(R.id.swiperefreshLayout);
        swipeRefreshLayout.setOnRefreshListener(() -> loadWaitingOrder(new SessionManager(getActivity()).getAccountApotik().getPharmacy_id()));
        empty = view.findViewById(R.id.empty_linearlayout);

        adapter_siapdikirim = new Adapter_siapdikirim(view.getContext(),finishOrders,this);
        recyclerView_dikirim.setLayoutManager(new LinearLayoutManager(view.getContext(),RecyclerView.VERTICAL,false));
        recyclerView_dikirim.setAdapter(adapter_siapdikirim);
    }

    private void loadWaitingOrder(int pharmacy_id) {
        JSONObject json = new JSONObject();

        try {
            json.put("pharmacy_id", pharmacy_id);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Log.d("json", json.toString());

        Call<LoadDikirimResponse> call;
        API service = Client.getAPI();
        call = service.loadwaitingorder(json.toString());

        call.enqueue(new Callback<LoadDikirimResponse>() {
            @Override
            public void onResponse(@NonNull Call<LoadDikirimResponse> call, @NonNull
                    Response<LoadDikirimResponse> response) {
                if(response.isSuccessful()) {
                    LoadDikirimResponse result =response.body();
                    if(!result.isError()) {
                        finishOrders.clear();
                        finishOrders.addAll(result.getFinishOrders());
                        adapter_siapdikirim.notifyDataSetChanged();
                        if(result.getFinishOrders().size()==0){
                            empty.setVisibility(View.VISIBLE);
                        }
                    }else{
                        Toast.makeText(getActivity(), "Load medicine failed", Toast.LENGTH_SHORT).show();
                    }
                    swipeRefreshLayout.setRefreshing(false);
                }
            }

            @Override
            public void onFailure(@NonNull Call<LoadDikirimResponse> call, @NonNull Throwable t) {
                t.printStackTrace();
                Toast.makeText(getActivity(), getString(R.string.koneksiTidakStabil),
                        Toast.LENGTH_SHORT).show();
            }
        });
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void TelfonCallback(String number) {
        Intent intent = new Intent(Intent.ACTION_DIAL);
        intent.setData(Uri.parse("tel:"+number));
        startActivity(intent);
    }

    @Override
    public void onPressedInfoCourier(int transaction_id) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Masukkan info kurir");

        final LinearLayout linearLayout = new LinearLayout(getActivity());
        linearLayout.setOrientation(LinearLayout.VERTICAL);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        layoutParams.setMargins(20, 0, 20, 0);
        linearLayout.setLayoutParams(layoutParams);

        final TextView textView = new TextView(getActivity());
        textView.setLayoutParams(new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT));
        textView.setText("*harap memasukan nomor resi pengiriman");
        textView.setTextColor(getActivity().getResources().getColor(R.color.red_orange));

        // Set up the input
        final EditText input = new EditText(getActivity());
        // Specify the type of input expected; this, for example, sets the input as a password, and will mask the text
        input.setInputType(InputType.TYPE_CLASS_TEXT);

        linearLayout.addView(input);
        linearLayout.addView(textView);
        builder.setView(linearLayout);

// Set up the buttons
        builder.setPositiveButton("Kirim Resi", (dialog, which) -> {
            if(input.getText().toString().equals("")){
                Toast.makeText(getActivity(), "Nomor Resi tidak boleh kosong", Toast.LENGTH_SHORT).show();
            }else {
                KirimResi(transaction_id,input.getText().toString());
            }
        });
        builder.setNegativeButton("Cancel", (dialog, which) -> dialog.cancel());

        builder.show();
    }

    private void KirimResi(int transaction_id, String resi) {
        JSONObject json = new JSONObject();

        try {
            json.put("transaction_id", transaction_id);
            json.put("resi",resi);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Log.d("json", json.toString());

        Call<BaseResponse> call;
        API service = Client.getAPI();
        call = service.sendcourierinfo(json.toString());

        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(@NonNull Call<BaseResponse> call, @NonNull
                    Response<BaseResponse> response) {
                if(response.isSuccessful()) {
                    BaseResponse result =response.body();
                    if(!result.isError()) {
                        loadWaitingOrder(new SessionManager(getActivity()).getAccountApotik().getPharmacy_id());
                        Toast.makeText(getActivity(), result.getMessage(), Toast.LENGTH_SHORT).show();
                    }else{
                        Toast.makeText(getActivity(), "setCourierInfo failed", Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<BaseResponse> call, @NonNull Throwable t) {
                t.printStackTrace();
                Toast.makeText(getActivity(), getString(R.string.koneksiTidakStabil),
                        Toast.LENGTH_SHORT).show();
            }
        });

    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
