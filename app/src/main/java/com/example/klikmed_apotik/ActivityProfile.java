package com.example.klikmed_apotik;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.example.klikmed_apotik.model.Apotik;
import com.example.klikmed_apotik.session.SessionManager;

public class ActivityProfile extends AppCompatActivity {
    TextView pharmacy_name,pharmacy_number,pharmacy_address;
    ImageView pharmacy_logo;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        pharmacy_name = findViewById(R.id.pharmacy_name);
        pharmacy_logo = findViewById(R.id.pharmacy_logo);
        pharmacy_number = findViewById(R.id.pharmacy_number);
        pharmacy_address = findViewById(R.id.pharmacy_address);

        Apotik apotik = new SessionManager(this).getAccountApotik();
        pharmacy_name.setText(apotik.getPharmacy_name());
        pharmacy_number.setText(apotik.getPharmacy_number());
        pharmacy_address.setText(apotik.getPharmacy_address());

        Glide.with(this)
                .load(apotik.getPharmacy_logo())
                .placeholder(R.color.colorAbuAbu)
                .error(R.color.colorAbuAbu)
                .diskCacheStrategy(DiskCacheStrategy.NONE)
                .into(pharmacy_logo);
    }

    public void onbackpressed(View view) {
        super.onBackPressed();
    }

    public void onlogout(View view) {
        Intent i= new Intent(this,ActivityMain.class);
        startActivity(i);
        new SessionManager(this).removeAccount();
        ActivityCompat.finishAffinity(this);
    }
}
