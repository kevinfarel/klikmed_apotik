package com.example.klikmed_apotik;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.example.klikmed_apotik.Adapter.Adapter_sukses;
import com.example.klikmed_apotik.model.FinishOrder;
import com.example.klikmed_apotik.model.SuccessOrder;
import com.example.klikmed_apotik.retrofit.API;
import com.example.klikmed_apotik.retrofit.Client;
import com.example.klikmed_apotik.retrofit.Response.LoadDikirimResponse;
import com.example.klikmed_apotik.retrofit.Response.LoadSuksesResponse;
import com.example.klikmed_apotik.session.SessionManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link FragmentSelesai.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link FragmentSelesai#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FragmentSelesai extends Fragment implements Adapter_sukses.AdapterCallback {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public FragmentSelesai() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment FragmentSelesai.
     */
    // TODO: Rename and change types and number of parameters
    public static FragmentSelesai newInstance(String param1, String param2) {
        FragmentSelesai fragment = new FragmentSelesai();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_selesai, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initComponent(view);
        loadSuccessOrder(new SessionManager(getActivity()).getAccountApotik().getPharmacy_id());
    }

    private void loadSuccessOrder(int pharmacy_id) {
        JSONObject json = new JSONObject();

        try {
            json.put("pharmacy_id", pharmacy_id);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Log.d("json", json.toString());

        Call<LoadSuksesResponse> call;
        API service = Client.getAPI();
        call = service.loadsuksesorder(json.toString());

        call.enqueue(new Callback<LoadSuksesResponse>() {
            @Override
            public void onResponse(@NonNull Call<LoadSuksesResponse> call, @NonNull
                    Response<LoadSuksesResponse> response) {
                if(response.isSuccessful()) {
                    LoadSuksesResponse result =response.body();
                    if(!result.isError()) {
                        successOrders.clear();
                        successOrders.addAll(result.getSuccessOrders());
                        adapter_sukses.notifyDataSetChanged();
                        if(result.getSuccessOrders().size()==0){
                            empty.setVisibility(View.VISIBLE);
                        }else{
                            empty.setVisibility(View.GONE);
                        }
                    }else{
                        Toast.makeText(getActivity(), "Load medicine failed", Toast.LENGTH_SHORT).show();
                    }
                    swipeRefreshLayout.setRefreshing(false);
                }
            }

            @Override
            public void onFailure(@NonNull Call<LoadSuksesResponse> call, @NonNull Throwable t) {
                t.printStackTrace();
                Toast.makeText(getActivity(), getString(R.string.koneksiTidakStabil),
                        Toast.LENGTH_SHORT).show();
            }
        });
    }

    RecyclerView recyclerView_sukses;
    SwipeRefreshLayout swipeRefreshLayout;
    Adapter_sukses adapter_sukses;
    LinearLayout empty;
    ArrayList<SuccessOrder> successOrders = new ArrayList<>();
    private void initComponent(View view) {
        recyclerView_sukses = view.findViewById(R.id.recyclerview_success);
        swipeRefreshLayout = view.findViewById(R.id.swiperefreshLayout);
        swipeRefreshLayout.setOnRefreshListener(() -> loadSuccessOrder(new SessionManager(view.getContext()).getAccountApotik().getPharmacy_id()));
        empty = view.findViewById(R.id.empty_linearlayout);

        adapter_sukses = new Adapter_sukses(view.getContext(),successOrders,this);
        recyclerView_sukses.setLayoutManager(new LinearLayoutManager(view.getContext(),RecyclerView.VERTICAL,false));
        recyclerView_sukses.setAdapter(adapter_sukses);
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void TelfonCallback(String number) {
        Intent intent = new Intent(Intent.ACTION_DIAL);
        intent.setData(Uri.parse("tel:"+number));
        startActivity(intent);
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
