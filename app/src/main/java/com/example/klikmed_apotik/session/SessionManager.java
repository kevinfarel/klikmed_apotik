package com.example.klikmed_apotik.session;

import android.content.Context;
import android.content.SharedPreferences;
import com.example.klikmed_apotik.model.Apotik;
import com.google.gson.Gson;

public class SessionManager {

    private static final String PREFER_NAME = "Session";
    private static final String APOTIK_DATA = "ApotikData";

    SharedPreferences pref;
    SharedPreferences.Editor editor;

    public SessionManager(Context context) {

        int PRIVATE_MODE = 0;
        this.pref = context.getSharedPreferences(PREFER_NAME, PRIVATE_MODE);
        editor = pref.edit();
        editor.apply();
    }
//
    public void saveAccountApotik(Apotik apotik) {

        Gson gson = new Gson();

        String jsonAccount = gson.toJson(apotik);

        editor.putString(APOTIK_DATA, jsonAccount);

        editor.commit();
    }
//
    public Apotik getAccountApotik() {

        Apotik apotik;

        String jsonAccounts = pref.getString(APOTIK_DATA, null);

        if (jsonAccounts != null) {

            Gson gson = new Gson();

            apotik = gson.fromJson(jsonAccounts, Apotik.class);

            return apotik;

        } else {
            return null;
        }
    }


    public void removeAccount() {

        editor.clear();
        editor.apply();
    }
}
