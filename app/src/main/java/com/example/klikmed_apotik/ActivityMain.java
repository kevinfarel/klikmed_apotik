package com.example.klikmed_apotik;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import com.example.klikmed_apotik.session.SessionManager;

public class ActivityMain extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        if(new SessionManager(this).getAccountApotik()!=null){
            Intent i = new Intent(this,ActivityHome.class);
            startActivity(i);
            ActivityCompat.finishAffinity(this);
        }
    }

    public void Masuk(View view) {
        Intent i = new Intent(this,ActivityLogin.class);
        startActivity(i);
    }

    public void Daftar(View view) {
        Intent i = new Intent(this,ActivityRegister.class);
        startActivity(i);
    }
}
