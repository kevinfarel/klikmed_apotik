package com.example.klikmed_apotik;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.klikmed_apotik.Adapter.Adapter_obat;
import com.example.klikmed_apotik.Adapter.Adapter_pilihobat;
import com.example.klikmed_apotik.model.Apotik;
import com.example.klikmed_apotik.model.Medicine;
import com.example.klikmed_apotik.model.PharmacyMedicine;
import com.example.klikmed_apotik.retrofit.API;
import com.example.klikmed_apotik.retrofit.Client;
import com.example.klikmed_apotik.retrofit.Response.BaseResponse;
import com.example.klikmed_apotik.retrofit.Response.LoadMedicineResponse;
import com.example.klikmed_apotik.retrofit.Response.LoadSuksesResponse;
import com.example.klikmed_apotik.retrofit.Response.MedicineListResponse;
import com.example.klikmed_apotik.session.SessionManager;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.view.View.GONE;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link FragmentTambahObat.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link FragmentTambahObat#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FragmentTambahObat extends Fragment implements Adapter_obat.AdapterCallback,Adapter_pilihobat.AdapterCallback {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public FragmentTambahObat() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment FragmentTambahObat.
     */
    // TODO: Rename and change types and number of parameters
    public static FragmentTambahObat newInstance(String param1, String param2) {
        FragmentTambahObat fragment = new FragmentTambahObat();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_tambah_obat, container, false);
    }
    Apotik apotik;
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        apotik = new SessionManager(view.getContext()).getAccountApotik();
        initComponent(view);
        loadMedicine(apotik.getPharmacy_id());
    }

    private void loadMedicine(int pharmacy_id) {
        JSONObject json = new JSONObject();

        try {
            json.put("pharmacy_id", pharmacy_id);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Log.d("json", json.toString());

        Call<LoadMedicineResponse> call;
        API service = Client.getAPI();
        call = service.loadmedicines(json.toString());

        call.enqueue(new Callback<LoadMedicineResponse>() {
            @Override
            public void onResponse(@NonNull Call<LoadMedicineResponse> call, @NonNull
                    Response<LoadMedicineResponse> response) {
                if(response.isSuccessful()) {
                    LoadMedicineResponse result =response.body();
                    if(!result.isError()) {
                        medicines.clear();
                        medicines.addAll(result.getMedicines());
                        adapter_obat.notifyDataSetChanged();
                        if(result.getMedicines().size()==0){
                            empty.setVisibility(View.VISIBLE);
                        }else{
                            empty.setVisibility(GONE);
                        }
                    }else{
                        Toast.makeText(getActivity(), "Load medicine failed", Toast.LENGTH_SHORT).show();
                    }
                    swipeRefreshLayout.setRefreshing(false);
                }
            }

            @Override
            public void onFailure(@NonNull Call<LoadMedicineResponse> call, @NonNull Throwable t) {
                t.printStackTrace();
                Toast.makeText(getActivity(), getString(R.string.koneksiTidakStabil),
                        Toast.LENGTH_SHORT).show();
            }
        });
    }

    RecyclerView recyclerView_medicine;
    SwipeRefreshLayout swipeRefreshLayout;
    Adapter_obat adapter_obat;
    LinearLayout empty;
    FloatingActionButton fab;
    TextView tambah_obat_text;
    ArrayList<PharmacyMedicine> medicines = new ArrayList<>();
    private void initComponent(View view) {
        tambah_obat_text = view.findViewById(R.id.tambah_obat_text);
        tambah_obat_text.setOnClickListener(view1 -> tambahObat(view));
        recyclerView_medicine = view.findViewById(R.id.recyclerview_medicines);
        swipeRefreshLayout = view.findViewById(R.id.swiperefreshLayout);
        swipeRefreshLayout.setOnRefreshListener(() -> loadMedicine(new SessionManager(view.getContext()).getAccountApotik().getPharmacy_id()));
        empty = view.findViewById(R.id.empty_linearlayout);
        fab = view.findViewById(R.id.fab);
        fab.setOnClickListener(this::tambahObat);

        adapter_obat = new Adapter_obat(view.getContext(),medicines,this);
        recyclerView_medicine.setLayoutManager(new LinearLayoutManager(view.getContext(),RecyclerView.VERTICAL,false));
        recyclerView_medicine.setAdapter(adapter_obat);
    }

    private void tambahObat(View view){
        showAddMedicineDialog(view);
    }
    TextView add_medicine_button;
    EditText medicine_name,medicine_price,medicine_stock;
    int selected_medicine_id=-1;
    private void showAddMedicineDialog(View view) {
        BottomSheetDialog mBottomSheetDialog = new BottomSheetDialog(view.getContext());
        View sheetView = getLayoutInflater().inflate(R.layout.dialog_add_medicine, null);
        mBottomSheetDialog.setContentView(sheetView);
        medicine_name = sheetView.findViewById(R.id.medicine_name);
        medicine_name.setOnClickListener(view1 -> showDialogPilihObat(view1));
        medicine_price = sheetView.findViewById(R.id.medicine_price);
        medicine_stock = sheetView.findViewById(R.id.stock);
        medicine_price.addTextChangedListener(new TextWatcher(){
            public void onTextChanged(CharSequence s, int start, int before, int count)
            {
                if (medicine_price.getText().toString().matches("^0") )
                    medicine_price.setText("");
            }
            @Override
            public void afterTextChanged(Editable arg0) { }
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
        });
        medicine_stock.addTextChangedListener(new TextWatcher(){
            public void onTextChanged(CharSequence s, int start, int before, int count)
            {
                if (medicine_stock.getText().toString().matches("^0") )
                    medicine_stock.setText("");
            }
            @Override
            public void afterTextChanged(Editable arg0) { }
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
        });
        add_medicine_button = sheetView.findViewById(R.id.tambah_obat_button);
        add_medicine_button.setOnClickListener(view1 -> {
            if(validate_obat(view1)){
                mBottomSheetDialog.dismiss();
                int price = Integer.valueOf(medicine_price.getText().toString());
                int pharmacy_id = new SessionManager(view.getContext()).getAccountApotik().getPharmacy_id();
                int stock = Integer.valueOf(medicine_stock.getText().toString());
                createmedicine(selected_medicine_id,price,stock,pharmacy_id);
            }
        });
        mBottomSheetDialog.show();
    }
    Dialog dialog;
    LayoutInflater inflater;
    View dialogView;
    EditText search;
    ProgressBar search_obat_loading;
    TextView empty_list;
    RecyclerView recyclerView_obat;
    Adapter_pilihobat adapter_pilihobat;
    ArrayList<Medicine> medicineArrayList = new ArrayList<>();

    private void showDialogPilihObat(View view1) {
        medicineArrayList.clear();
        dialog = new Dialog(view1.getContext());
        inflater = getLayoutInflater();
        dialogView = inflater.inflate(R.layout.dialog_pilih_obat,null );
        dialog.setContentView(dialogView);
        dialog.setCancelable(true);
        dialog.setTitle("Pilih Obat");
        ImageView close = dialogView.findViewById(R.id.close);
        close.setOnClickListener(view -> dialog.dismiss());
        ImageView search_btn = dialogView.findViewById(R.id.search_btn);
        search_btn.setOnClickListener(view -> {
            if(search.getText().toString().length()!=0){
                search_medicine(search.getText().toString(),apotik.getPharmacy_id());
            }
        });
        search = dialogView.findViewById(R.id.search_obat);
        search.setOnEditorActionListener((textView, i, keyEvent) -> {
            if (i == EditorInfo.IME_ACTION_SEARCH) {
                if(search.getText().toString().length()!=0){
                    search_medicine(search.getText().toString(),apotik.getPharmacy_id());
                }
                return true;
            }
            return false;
        });
        search_obat_loading = dialogView.findViewById(R.id.search_obat_loading);
        empty_list = dialogView.findViewById(R.id.empty_list);
        recyclerView_obat = dialogView.findViewById(R.id.recyclerview_obat);
        adapter_pilihobat = new Adapter_pilihobat(view1.getContext(),medicineArrayList, this);
        recyclerView_obat.setLayoutManager(new LinearLayoutManager(view1.getContext(),RecyclerView.VERTICAL,false));
        recyclerView_obat.setAdapter(adapter_pilihobat);
        dialog.show();
    }

    private void search_medicine(String search_text, int pharmacy_id) {
        recyclerView_obat.setVisibility(GONE);
        empty_list.setVisibility(GONE);
        search_obat_loading.setVisibility(View.VISIBLE);
        JSONObject json = new JSONObject();
        try {
            json.put("search_text", search_text);
            json.put("pharmacy_id",pharmacy_id);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Log.d("json", json.toString());

        Call<MedicineListResponse> call;
        API service = Client.getAPI();
        call = service.searchmedicine(json.toString());

        call.enqueue(new Callback<MedicineListResponse>() {
            @Override
            public void onResponse(@NonNull Call<MedicineListResponse> call, @NonNull
                    Response<MedicineListResponse> response) {
                if(response.isSuccessful()) {
                    MedicineListResponse result =response.body();
                    assert result != null;
                    if(!result.isError()){
                        if(result.getMedicines().isEmpty()){
                            empty_list.setVisibility(View.VISIBLE);
                        }else{
                            recyclerView_obat.setVisibility(View.VISIBLE);
                            medicineArrayList.clear();
                            medicineArrayList.addAll(result.getMedicines());
                            adapter_pilihobat.notifyDataSetChanged();
                        }
                    }else{
                        Toast.makeText(getActivity(), result.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }else{
                    Toast.makeText(getActivity(), "Request Failed", Toast.LENGTH_SHORT).show();
                    empty_list.setVisibility(View.VISIBLE);
                }
                search_obat_loading.setVisibility(GONE);
            }

            @Override
            public void onFailure(@NonNull Call<MedicineListResponse> call, @NonNull Throwable t) {
                t.printStackTrace();
                Toast.makeText(getActivity(), getString(R.string.koneksiTidakStabil),
                        Toast.LENGTH_SHORT).show();
                search_obat_loading.setVisibility(GONE);
                empty_list.setVisibility(View.VISIBLE);
            }
        });
    }

    private void createmedicine(int medicine_id, int medicine_price, int stock, int pharmacy_id) {
        ProgressDialog progressDialog = ProgressDialog.show(getActivity(), "",
                "Adding Medicine...", true);
        progressDialog.show();
        JSONObject json = new JSONObject();

        try {
            json.put("medicine_id", medicine_id);
            json.put("medicine_price", medicine_price);
            json.put("stock", stock);
            json.put("pharmacy_id", pharmacy_id);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Log.d("json", json.toString());

        Call<BaseResponse> call;
        API service = Client.getAPI();
        call = service.createmedicine(json.toString());

        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(@NonNull Call<BaseResponse> call, @NonNull
                    Response<BaseResponse> response) {
                if(response.isSuccessful()) {
                    BaseResponse result =response.body();
                    Toast.makeText(getActivity(), result.getMessage(), Toast.LENGTH_SHORT).show();
                }
                progressDialog.dismiss();
                loadMedicine(new SessionManager(getActivity()).getAccountApotik().getPharmacy_id());
            }

            @Override
            public void onFailure(@NonNull Call<BaseResponse> call, @NonNull Throwable t) {
                t.printStackTrace();
                Toast.makeText(getActivity(), getString(R.string.koneksiTidakStabil),
                        Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
            }
        });
    }

    private boolean validate_obat(View view) {
        if(selected_medicine_id==-1){
            Toast.makeText(view.getContext(), "Pilih obat yang ingin ditambahkan", Toast.LENGTH_SHORT).show();
            return false;
        }
        if(medicine_price.getText().toString().equals("") || medicine_price.getText().toString().isEmpty()){
            Toast.makeText(view.getContext(), "Harga harus diisi", Toast.LENGTH_SHORT).show();
            return false;
        }
        if(Integer.valueOf(medicine_price.getText().toString())<500){
            Toast.makeText(view.getContext(), "Harga minimal Rp. 500", Toast.LENGTH_SHORT).show();
            return false;
        }
        if(medicine_stock.getText().toString().equals("") || medicine_price.getText().toString().isEmpty()){
            Toast.makeText(view.getContext(), "Stok harus diisi", Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }


    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onDelete(int pharmacy_medicine_id,String medicine_name) {
        androidx.appcompat.app.AlertDialog.Builder builder = new androidx.appcompat.app.AlertDialog.Builder(Objects.requireNonNull(getActivity()));
        builder.setMessage(medicine_name)
                .setTitle("Konfirmasi hapus obat")
                .setNegativeButton("Batal", (dialog, id) -> dialog.dismiss())
                .setCancelable(false)
                .setPositiveButton("Hapus", (dialog, id) -> deletemedicine(pharmacy_medicine_id));
        AlertDialog alert = builder.create();
        alert.show();
    }

    private void deletemedicine(int pharmacy_medicine_id) {
        ProgressDialog progressDialog = ProgressDialog.show(getActivity(), "",
                "Removing Medicine...", true);
        progressDialog.show();
        JSONObject json = new JSONObject();

        try {
            json.put("pharmacy_medicine_id", pharmacy_medicine_id);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Log.d("json", json.toString());

        Call<BaseResponse> call;
        API service = Client.getAPI();
        call = service.deletemedicine(json.toString());

        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(@NonNull Call<BaseResponse> call, @NonNull
                    Response<BaseResponse> response) {
                if(response.isSuccessful()) {
                    BaseResponse result =response.body();
                    Toast.makeText(getActivity(), result.getMessage(), Toast.LENGTH_SHORT).show();
                }
                progressDialog.dismiss();
                loadMedicine(new SessionManager(getActivity()).getAccountApotik().getPharmacy_id());
            }

            @Override
            public void onFailure(@NonNull Call<BaseResponse> call, @NonNull Throwable t) {
                t.printStackTrace();
                Toast.makeText(getActivity(), getString(R.string.koneksiTidakStabil),
                        Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
            }
        });
    }

    @Override
    public void onEdit(View view,int pharmacy_medicine_id,int stock, String medicine_name, String medicine_type, int medicine_price) {
        showEditMedicineDialog(view,pharmacy_medicine_id,medicine_name,stock,medicine_price);
    }

    TextView edit_medicine_button,name;
    EditText edit_price,edit_stock;
    private void showEditMedicineDialog(View view,int pharmacy_medicine_id,String medicine_name, int medicine_stock, int medicine_price) {
        BottomSheetDialog mBottomSheetDialog = new BottomSheetDialog(view.getContext());
        View sheetView = getLayoutInflater().inflate(R.layout.dialog_edit_medicine, null);
        mBottomSheetDialog.setContentView(sheetView);
        name = sheetView.findViewById(R.id.medicine_name);
        name.setText(medicine_name);
        edit_price = sheetView.findViewById(R.id.medicine_price);
        edit_price.setText(String.valueOf(medicine_price));
        edit_stock = sheetView.findViewById(R.id.stock);
        edit_stock.setText(String.valueOf(medicine_stock));
        edit_price.addTextChangedListener(new TextWatcher(){
            public void onTextChanged(CharSequence s, int start, int before, int count)
            {
                if (edit_price.getText().toString().matches("^0") )
                    edit_price.setText("");
            }
            @Override
            public void afterTextChanged(Editable arg0) { }
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
        });
        edit_stock.addTextChangedListener(new TextWatcher(){
            public void onTextChanged(CharSequence s, int start, int before, int count)
            {
                if (edit_stock.getText().toString().matches("^0") )
                    edit_stock.setText("");
            }
            @Override
            public void afterTextChanged(Editable arg0) { }
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
        });
        edit_medicine_button = sheetView.findViewById(R.id.edit_obat_button);
        edit_medicine_button.setOnClickListener(view1 -> {
            if(validate_edit_obat(view1)){
                mBottomSheetDialog.dismiss();
                int price = Integer.valueOf(edit_price.getText().toString());
                int stock = Integer.valueOf(edit_stock.getText().toString());
                editmedicine(pharmacy_medicine_id,price,stock);
            }
        });
        mBottomSheetDialog.show();
    }

    private void editmedicine(int pharmacy_medicine_id, int medicine_price, int medicine_stock) {
        ProgressDialog progressDialog = ProgressDialog.show(getActivity(), "",
                "Mohon Tunggu", true);
        progressDialog.show();
        JSONObject json = new JSONObject();

        try {
            json.put("pharmacy_medicine_id", pharmacy_medicine_id);
            json.put("medicine_price",medicine_price);
            json.put("medicine_stock",medicine_stock);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Log.d("json", json.toString());

        Call<BaseResponse> call;
        API service = Client.getAPI();
        call = service.editmedicine(json.toString());

        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(@NonNull Call<BaseResponse> call, @NonNull
                    Response<BaseResponse> response) {
                if(response.isSuccessful()) {
                    BaseResponse result =response.body();
                    Toast.makeText(getActivity(), result.getMessage(), Toast.LENGTH_SHORT).show();
                }
                progressDialog.dismiss();
                loadMedicine(new SessionManager(getActivity()).getAccountApotik().getPharmacy_id());
            }

            @Override
            public void onFailure(@NonNull Call<BaseResponse> call, @NonNull Throwable t) {
                t.printStackTrace();
                Toast.makeText(getActivity(), getString(R.string.koneksiTidakStabil),
                        Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
            }
        });
    }

    private boolean validate_edit_obat(View view) {
        if(edit_price.getText().toString().equals("") || edit_price.getText().toString().isEmpty()){
            Toast.makeText(view.getContext(), "Harga harus diisi", Toast.LENGTH_SHORT).show();
            return false;
        }
        if(Integer.valueOf(edit_price.getText().toString())<500){
            Toast.makeText(view.getContext(), "Harga minimal Rp. 500", Toast.LENGTH_SHORT).show();
            return false;
        }
        if(edit_stock.getText().toString().equals("") || edit_stock.getText().toString().isEmpty()){
            Toast.makeText(view.getContext(), "Stok harus diisi", Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }
    @Override
    public void onObatDipilih(Medicine m) {
        medicine_name.setText(m.getMedicine_name() + '('+m.getMedicine_type()+')');
        selected_medicine_id=m.getMedicine_id();
        dialog.dismiss();
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
