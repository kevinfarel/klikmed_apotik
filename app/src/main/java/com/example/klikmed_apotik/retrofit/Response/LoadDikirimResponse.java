package com.example.klikmed_apotik.retrofit.Response;

import com.example.klikmed_apotik.model.FinishOrder;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class LoadDikirimResponse extends BaseResponse{
    @SerializedName("transaction")
    ArrayList<FinishOrder> finishOrders;

    public ArrayList<FinishOrder> getFinishOrders() {
        return finishOrders;
    }
}
