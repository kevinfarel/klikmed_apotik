package com.example.klikmed_apotik.retrofit.Response;

import com.example.klikmed_apotik.model.Apotik;
import com.google.gson.annotations.SerializedName;

public class LoginResponse extends BaseResponse {
    @SerializedName("pharmacy")
    private Apotik pharmacy;

    public Apotik getPharmacy() {
        return pharmacy;
    }
}
