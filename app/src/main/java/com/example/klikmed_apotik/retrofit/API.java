package com.example.klikmed_apotik.retrofit;

import com.example.klikmed_apotik.retrofit.Response.BaseResponse;
import com.example.klikmed_apotik.retrofit.Response.LoadDikirimResponse;
import com.example.klikmed_apotik.retrofit.Response.LoadMedicineResponse;
import com.example.klikmed_apotik.retrofit.Response.LoadOrderResponse;
import com.example.klikmed_apotik.retrofit.Response.LoadSuksesResponse;
import com.example.klikmed_apotik.retrofit.Response.LoginResponse;
import com.example.klikmed_apotik.retrofit.Response.MedicineListResponse;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

public interface API {
    //login apotik
    @FormUrlEncoded
    @POST("api/loginpharmacy")
    Call<LoginResponse> login(@Field("json") String json);

    @FormUrlEncoded
    @POST("api/rejectorder")
    Call<BaseResponse> rejectorder(@Field("json") String json);

    @FormUrlEncoded
    @POST("api/loadorder")
    Call<LoadOrderResponse> loadorder(@Field("json") String json);

    @FormUrlEncoded
    @POST("api/loadmedicine")
    Call<LoadMedicineResponse> loadmedicines(@Field("json") String json);

//    @FormUrlEncoded
//    @POST("api/getorder")
//    Call<BaseResponse> selesaiterimaorder(@Field("json") String json);
    @FormUrlEncoded
    @POST("api/getorder")
    Call<BaseResponse> terimaorder(@Field("json") String json);


    @FormUrlEncoded
    @POST("api/waitingorder")
    Call<LoadDikirimResponse> loadwaitingorder(@Field("json") String json);

    @FormUrlEncoded
    @POST("api/updatecourier")
    Call<BaseResponse> sendcourierinfo(@Field("json") String json);

    @FormUrlEncoded
    @POST("api/successorder")
    Call<LoadSuksesResponse> loadsuksesorder(@Field("json") String json);

    @FormUrlEncoded
    @POST("api/deletemedicine")
    Call<BaseResponse> deletemedicine(@Field("json") String json);

    @FormUrlEncoded
    @POST("api/createmedicine")
    Call<BaseResponse> createmedicine(@Field("json") String json);

    @Multipart
    @POST("api/registerpharmacy")
    Call<BaseResponse> registerpharmacy(@Part("json") RequestBody json, @Part MultipartBody.Part image);

    @FormUrlEncoded
    @POST("api/searchmedicine")
    Call<MedicineListResponse> searchmedicine(@Field("json") String toString);

    @FormUrlEncoded
    @POST("api/editmedicine")
    Call<BaseResponse> editmedicine(@Field("json") String toString);
}
