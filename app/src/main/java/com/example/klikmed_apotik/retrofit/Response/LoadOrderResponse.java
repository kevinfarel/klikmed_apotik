package com.example.klikmed_apotik.retrofit.Response;

import com.example.klikmed_apotik.model.Order;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class LoadOrderResponse extends BaseResponse {
    @SerializedName("orders")
    ArrayList<Order> orders;

    public ArrayList<Order> getOrders() {
        return orders;
    }
}
