package com.example.klikmed_apotik.retrofit.Response;

import com.google.gson.annotations.SerializedName;

public class BaseResponse {
    @SerializedName("error")
    private boolean error;

    @SerializedName("message")
    private String message;

    public boolean isError() {
        return error;
    }

    public String getMessage() {
        return message;
    }
}
