package com.example.klikmed_apotik.retrofit.Response;

import com.example.klikmed_apotik.model.SuccessOrder;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class LoadSuksesResponse extends BaseResponse{
    @SerializedName("transaction")
    ArrayList<SuccessOrder> successOrders;

    public ArrayList<SuccessOrder> getSuccessOrders() {
        return successOrders;
    }
}
