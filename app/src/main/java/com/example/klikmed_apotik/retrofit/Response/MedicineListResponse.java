package com.example.klikmed_apotik.retrofit.Response;
import com.example.klikmed_apotik.model.Medicine;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class MedicineListResponse extends BaseResponse {
    @SerializedName("medicines")
    ArrayList<Medicine> medicines;

    public ArrayList<Medicine> getMedicines() {
        return medicines;
    }
}
