package com.example.klikmed_apotik.retrofit.Response;

import com.example.klikmed_apotik.model.PharmacyMedicine;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class LoadMedicineResponse extends BaseResponse {
    @SerializedName("medicine")
    ArrayList<PharmacyMedicine> medicines;

    public ArrayList<PharmacyMedicine> getMedicines() {
        return medicines;
    }
}
