package com.example.klikmed_apotik;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;

import java.io.IOException;
import java.util.List;

public class ActivityPilihLokasi extends AppCompatActivity implements OnMapReadyCallback {

    SupportMapFragment mapFragment;
    private GoogleMap mMap;
    private static final int MY_PERMISSIONS_REQUEST_LOCATION = 101;
    private static final int RequestPermissionCode = 1;
    private GoogleMap.OnCameraIdleListener onCameraIdleListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pilih_lokasi);
        mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        initComponent();
        configureCameraIdle();
    }
    TextView address_text,pilih;
    private void initComponent(){
        address_text=findViewById(R.id.address);
        pilih=findViewById(R.id.pilih);
        pilih.setOnClickListener(view -> sendResult(address_text.getText().toString()));
    }

    private void sendResult(String text) {
        if(!text.equals("Lokasi tidak tersedia")) {
            Intent intent = getIntent();
            intent.putExtra("address", text);
            intent.putExtra("latitude", latitude);
            intent.putExtra("longitude", longitude);
            setResult(RESULT_OK, intent);
            finish();
        }else{
            Toast.makeText(this, "Pilih lokasi yang tersedia", Toast.LENGTH_SHORT).show();
        }
    }

    double latitude,longitude;
    private void configureCameraIdle() {
        onCameraIdleListener = () -> {

            LatLng latLng = mMap.getCameraPosition().target;
            Geocoder geocoder = new Geocoder(ActivityPilihLokasi.this);

            try {
                List<Address> addressList = geocoder.getFromLocation(latLng.latitude, latLng.longitude, 1);
                if (addressList != null && addressList.size() > 0) {
                    String locality = addressList.get(0).getAddressLine(0);
                    String country = addressList.get(0).getCountryName();
                    latitude = addressList.get(0).getLatitude();
                    longitude = addressList.get(0).getLongitude();
                    if (locality!=null && country!=null) {
                        //resutText.setText(locality + "  " + country);
                        //Toast.makeText(this, locality + "  " + country, Toast.LENGTH_SHORT).show();
                        address_text.setText(locality);
                    }else {
                        address_text.setText("Lokasi tidak tersedia");
                    }
                }

            } catch (IOException e) {
                e.printStackTrace();
            }

        };
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.getUiSettings().setMyLocationButtonEnabled(true);
        mMap.setOnCameraIdleListener(onCameraIdleListener);
        mMap.setMyLocationEnabled(true);

        FusedLocationProviderClient mFusedLocation = LocationServices.getFusedLocationProviderClient(this);
        mFusedLocation.getLastLocation().addOnSuccessListener(this, location -> {
            if(location!=null){
//                LatLng currentLocation = new LatLng(location.getLatitude(), location.getLongitude());
//                mMap.moveCamera(CameraUpdateFactory.newLatLng(currentLocation));
                mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(location.getLatitude(), location.getLongitude()), 20.0f));
            }
        });
        // Add a marker in Sydney and move the camera
//        LatLng sydney = new LatLng(-34, 151);
//        mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));
    }
}
