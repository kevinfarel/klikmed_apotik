package com.example.klikmed_apotik;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.example.klikmed_apotik.Adapter.Adapter_Order;
import com.example.klikmed_apotik.model.Order;
import com.example.klikmed_apotik.retrofit.API;
import com.example.klikmed_apotik.retrofit.Client;
import com.example.klikmed_apotik.retrofit.Response.BaseResponse;
import com.example.klikmed_apotik.retrofit.Response.LoadOrderResponse;
import com.example.klikmed_apotik.session.SessionManager;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link FragmentOrder.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link FragmentOrder#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FragmentOrder extends Fragment implements Adapter_Order.AdapterCallback {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private OnFragmentInteractionListener mListener;

    public FragmentOrder() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment FragmentOrder.
     */
    // TODO: Rename and change types and number of parameters
    public static FragmentOrder newInstance(String param1, String param2) {
        FragmentOrder fragment = new FragmentOrder();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            // TODO: Rename and change types of parameters
            String mParam1 = getArguments().getString(ARG_PARAM1);
            String mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_order, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initComponent(view);
    }
    private SwipeRefreshLayout swipeRefreshLayout;
    private Adapter_Order adapter_order;
    private RecyclerView recyclerView;
    private ArrayList<Order> orderArrayList;
    private LinearLayout emptylist;
    private void initComponent(View view) {
        swipeRefreshLayout = view.findViewById(R.id.swiperefreshLayout);
        emptylist = view.findViewById(R.id.empty_linearlayout);
        orderArrayList = new ArrayList<>();
        adapter_order = new Adapter_Order(view.getContext(),orderArrayList,this);
        recyclerView = view.findViewById(R.id.recyclerview_order);
        recyclerView.setLayoutManager(new LinearLayoutManager(view.getContext(),RecyclerView.VERTICAL,false));
        recyclerView.setAdapter(adapter_order);
        loadorder(new SessionManager(view.getContext()).getAccountApotik().getPharmacy_id());
        swipeRefreshLayout.setOnRefreshListener(() -> loadorder(new SessionManager(view.getContext()).getAccountApotik().getPharmacy_id()));
    }

    private void loadorder(int pharmacy_id) {
        JSONObject json = new JSONObject();

        try {
            json.put("pharmacy_id", pharmacy_id);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Log.d("json", json.toString());

        Call<LoadOrderResponse> call;
        API service = Client.getAPI();
        call = service.loadorder(json.toString());

        call.enqueue(new Callback<LoadOrderResponse>() {
            @Override
            public void onResponse(@NonNull Call<LoadOrderResponse> call, @NonNull
                    Response<LoadOrderResponse> response) {
                if(response.isSuccessful()) {
                    LoadOrderResponse result =response.body();
                    if(!result.isError()){
                        orderArrayList.clear();
                        if(result.getOrders().size()==0){
                            recyclerView.setVisibility(View.GONE);
                            emptylist.setVisibility(View.VISIBLE);
                        }else{
                            recyclerView.setVisibility(View.VISIBLE);
                            emptylist.setVisibility(View.GONE);
                            orderArrayList.addAll(result.getOrders());
                            adapter_order.notifyDataSetChanged();
                        }
                    }else{
                        Toast.makeText(getActivity(), "Load Order Failed", Toast.LENGTH_SHORT).show();
                    }
                }
                swipeRefreshLayout.setRefreshing(false);
            }

            @Override
            public void onFailure(@NonNull Call<LoadOrderResponse> call, @NonNull Throwable t) {
                t.printStackTrace();
                Toast.makeText(getActivity(), getString(R.string.koneksiTidakStabil),
                        Toast.LENGTH_SHORT).show();
                swipeRefreshLayout.setRefreshing(false);
            }
        });

    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void TolakCallback(int transaction_id) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Alasan Tolak Order");

// Set up the input
        final EditText input = new EditText(getActivity());
// Specify the type of input expected; this, for example, sets the input as a password, and will mask the text
        input.setInputType(InputType.TYPE_TEXT_FLAG_IME_MULTI_LINE);
        builder.setView(input);

// Set up the buttons
        builder.setPositiveButton("Tolak Order", (dialog, which) -> {
            if(input.getText().toString().equals("")){
                Toast.makeText(getActivity(), "Berikan alasan tolak order", Toast.LENGTH_SHORT).show();
            }else {
                TolakOrder(transaction_id, input.getText().toString());
            }
        });
        builder.setNegativeButton("Cancel", (dialog, which) -> dialog.cancel());

        builder.show();
    }

    @Override
    public void TerimaCallback(int transaction_id) {
        ProgressDialog progressDialog = ProgressDialog.show(getActivity(), "",
                "Menerima Order. Please wait...", true);
        progressDialog.show();
        JSONObject json = new JSONObject();

        try {
            json.put("transaction_id", transaction_id);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Log.d("json", json.toString());

        Call<BaseResponse> call;
        API service = Client.getAPI();
        call = service.terimaorder(json.toString());

        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(@NonNull Call<BaseResponse> call, @NonNull
                    Response<BaseResponse> response) {
                if(response.isSuccessful()) {
                    BaseResponse result =response.body();
                    Toast.makeText(getActivity(), result.getMessage(), Toast.LENGTH_SHORT).show();
                }
                progressDialog.dismiss();
                loadorder(new SessionManager(getActivity()).getAccountApotik().getPharmacy_id());
            }

            @Override
            public void onFailure(@NonNull Call<BaseResponse> call, @NonNull Throwable t) {
                t.printStackTrace();
                Toast.makeText(getActivity(), getString(R.string.koneksiTidakStabil),
                        Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
            }
        });
    }

//    @Override
//    public void TerimaCallback(int transaction_id, Order order) {
//        Intent i  = new Intent(getActivity(),ActivityTerimaOrder.class);
//        i.putExtra("transaction_id",transaction_id);
//        Gson gson = new Gson();
//        String orderjson = gson.toJson(order,Order.class);
//        i.putExtra("order",orderjson);
//        startActivity(i);
//    }

    @Override
    public void TelfonCallback(String phonenumber) {
        Intent intent = new Intent(Intent.ACTION_DIAL);
        intent.setData(Uri.parse("tel:"+phonenumber));
        startActivity(intent);
    }

    private void TolakOrder(int transaction_id, String reject_notes) {
        ProgressDialog progressDialog = ProgressDialog.show(getActivity(), "",
                "Rejecting. Please wait...", true);
        progressDialog.show();
        JSONObject json = new JSONObject();

        try {
            json.put("transaction_id", transaction_id);
            json.put("reject_notes",reject_notes);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Log.d("json", json.toString());

        Call<BaseResponse> call;
        API service = Client.getAPI();
        call = service.rejectorder(json.toString());

        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(@NonNull Call<BaseResponse> call, @NonNull
                    Response<BaseResponse> response) {
                if(response.isSuccessful()) {
                    BaseResponse result =response.body();
                    Toast.makeText(getActivity(), result.getMessage(), Toast.LENGTH_SHORT).show();
                }
                progressDialog.dismiss();
                loadorder(new SessionManager(getActivity()).getAccountApotik().getPharmacy_id());
            }

            @Override
            public void onFailure(@NonNull Call<BaseResponse> call, @NonNull Throwable t) {
                t.printStackTrace();
                Toast.makeText(getActivity(), getString(R.string.koneksiTidakStabil),
                        Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
            }
        });
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }


}
