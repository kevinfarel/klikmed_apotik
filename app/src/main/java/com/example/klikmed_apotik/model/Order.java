package com.example.klikmed_apotik.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class Order {
    @SerializedName("transaction_id")
    int transaction_id;

    @SerializedName("patient")
    Patient patient;

    @SerializedName("created_at")
    String created_at;

    @SerializedName("recipes")
    ArrayList<Recipe> recipes;

    public int getTransaction_id() {
        return transaction_id;
    }

    public Patient getPatient() {
        return patient;
    }

    public String getCreated_at() {
        return created_at;
    }

    public ArrayList<Recipe> getRecipes() {
        return recipes;
    }
}
