package com.example.klikmed_apotik.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class SuccessOrder {
    @SerializedName("transaction_id")
    int transaction_id;

    @SerializedName("transaction_status")
    int transaction_status;

    @SerializedName("patient")
    Patient patient;

    @SerializedName("total_price")
    int total_price;

    @SerializedName("updated_at")
    String updated_at;

    @SerializedName("medicines")
    ArrayList<MedicineOrders> medicines;

    public int getTransaction_id() {
        return transaction_id;
    }

    public int getTransaction_status() {
        return transaction_status;
    }

    public Patient getPatient() {
        return patient;
    }

    public ArrayList<MedicineOrders> getMedicines() {
        return medicines;
    }

    public int getTotal_price() {
        return total_price;
    }

    public String getUpdated_at() {
        return updated_at;
    }
}
