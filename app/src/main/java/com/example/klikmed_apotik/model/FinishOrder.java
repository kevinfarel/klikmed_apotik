package com.example.klikmed_apotik.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class FinishOrder {
    @SerializedName("transaction_id")
    int transaction_id;

    @SerializedName("transaction_status")
    int transaction_status;

    @SerializedName("courier_info")
    String courier_info;

    @SerializedName("patient")
    Patient patient;

    @SerializedName("created_at")
    String created_at;

    @SerializedName("medicines")
    ArrayList<MedicineOrders> medicines;

    public int getTransaction_id() {
        return transaction_id;
    }

    public int getTransaction_status() {
        return transaction_status;
    }

    public String getCourier_info() {
        return courier_info;
    }

    public Patient getPatient() {
        return patient;
    }

    public String getCreated_at() {
        return created_at;
    }

    public ArrayList<MedicineOrders> getMedicines() {
        return medicines;
    }
}
