package com.example.klikmed_apotik.model;

import com.google.gson.annotations.SerializedName;

public class Patient {
    @SerializedName("patient_id")
    int patient_id;

    @SerializedName("patient_first_name")
    String patient_first_name;

    @SerializedName("patient_last_name")
    String patient_last_name;

    @SerializedName("patient_address_detail")
    String alamat_detail;

    @SerializedName("latitude")
    double latitude;

    @SerializedName("longitude")
    double longitude;

    @SerializedName("patient_email")
    String patient_email;

    @SerializedName("patient_number")
    String patient_number;

    public int getPatient_id() {
        return patient_id;
    }

    public String getPatient_name() {
        return patient_first_name+" "+patient_last_name;
    }

    public String getAlamat_detail() {
        return alamat_detail;
    }

    public double getLatitude() {
        return latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public String getPatient_email() {
        return patient_email;
    }

    public String getPatient_number() {
        return patient_number;
    }
}
