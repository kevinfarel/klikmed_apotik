package com.example.klikmed_apotik.model;

import com.google.gson.annotations.SerializedName;

public class Recipe{
    @SerializedName("transaction_detail_id")
    int transaction_detail_id;

    @SerializedName("recipe_id")
    int recipe_id;

    @SerializedName("medicine_name")
    String medicine_name;

    @SerializedName("recipe_note")
    String recipe_note;

    @SerializedName("recipe_qty")
    int recipe_qty;

    public int getTransaction_detail_id() {
        return transaction_detail_id;
    }

    public int getRecipe_id() {
        return recipe_id;
    }

    public String getMedicine_name() {
        return medicine_name;
    }

    public String getRecipe_note() {
        return recipe_note;
    }

    public int getRecipe_qty() {
        return recipe_qty;
    }

}
