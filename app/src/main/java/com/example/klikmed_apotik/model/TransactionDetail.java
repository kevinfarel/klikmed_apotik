package com.example.klikmed_apotik.model;

import com.google.gson.annotations.SerializedName;

public class TransactionDetail {
    @SerializedName("transaction_detail_id")
    int transaction_detail_id;

    @SerializedName("medicine_id")
    int medicine_id;

    @SerializedName("price")
    int price;

    @SerializedName("qty")
    int qty;

    public void setTransaction_detail_id(int transaction_detail_id) {
        this.transaction_detail_id = transaction_detail_id;
    }

    public void setMedicine_id(int medicine_id) {
        this.medicine_id = medicine_id;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }
}
