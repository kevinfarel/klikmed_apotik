package com.example.klikmed_apotik.model;

import com.google.gson.annotations.SerializedName;

public class PharmacyMedicine extends Medicine{
    @SerializedName("pharmacy_medicine_id")
    int pharmacy_medicine_id;

    @SerializedName("pharmacy_id")
    int pharmacy_id;

    @SerializedName("medicine_qty")
    int medicine_qty;

    @SerializedName("medicine_price")
    int medicine_price;

    public int getPharmacy_medicine_id() {
        return pharmacy_medicine_id;
    }

    public int getPharmacy_id() {
        return pharmacy_id;
    }

    public int getMedicine_qty() {
        return medicine_qty;
    }

    public int getMedicine_price() {
        return medicine_price;
    }
}
