package com.example.klikmed_apotik.model;

import com.google.gson.annotations.SerializedName;

public class Apotik {
    @SerializedName("pharmacy_id")
    int pharmacy_id;
    @SerializedName("pharmacy_name")
    String pharmacy_name;
    @SerializedName("pharmacy_logo")
    String pharmacy_logo;
    @SerializedName("pharmacy_email")
    String pharmacy_email;
    @SerializedName("pharmacy_address")
    String pharmacy_address;
    @SerializedName("pharmacy_latitude")
    String pharmacy_latitude;
    @SerializedName("pharmacy_longitude")
    String pharmacy_longitude;
    @SerializedName("pharmacy_number")
    String pharmacy_number;
    @SerializedName("pharmacy_scndnumber")
    String pharmacy_scndnumber;

    public int getPharmacy_id() {
        return pharmacy_id;
    }

    public String getPharmacy_name() {
        return pharmacy_name;
    }

    public String getPharmacy_logo() {
        return pharmacy_logo;
    }

    public String getPharmacy_email() {
        return pharmacy_email;
    }

    public String getPharmacy_address() {
        return pharmacy_address;
    }

    public String getPharmacy_latitude() {
        return pharmacy_latitude;
    }

    public String getPharmacy_longitude() {
        return pharmacy_longitude;
    }

    public String getPharmacy_number() {
        return pharmacy_number;
    }

    public String getPharmacy_scndnumber() {
        return pharmacy_scndnumber;
    }
}
