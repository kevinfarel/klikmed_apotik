package com.example.klikmed_apotik.model;

import com.google.gson.annotations.SerializedName;

public class MedicineOrders extends Medicine {
    @SerializedName("qty")
    int medicine_qty;

    public int getMedicine_qty() {
        return medicine_qty;
    }
}
