package com.example.klikmed_apotik.model;

import com.google.gson.annotations.SerializedName;

public class Medicine {
    @SerializedName("medicine_id")
    int medicine_id;

    @SerializedName("medicine_name")
    String medicine_name;

    @SerializedName("medicine_type")
    String medicine_type;

    public int getMedicine_id() {
        return medicine_id;
    }

    public String getMedicine_name() {
        return medicine_name;
    }

    public String getMedicine_type() {
        return medicine_type;
    }
}
