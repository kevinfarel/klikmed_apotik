package com.example.klikmed_apotik;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.klikmed_apotik.retrofit.API;
import com.example.klikmed_apotik.retrofit.Client;
import com.example.klikmed_apotik.retrofit.Response.BaseResponse;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.Manifest.permission.ACCESS_FINE_LOCATION;

public class ActivityRegister extends AppCompatActivity {
    private static final int RequestPermissionCode = 101;
    private static final int REQUEST_ADDRESS_CODE =2 ;
    private static final int MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL = 1;
    private static final String TAG = ActivityRegister.class.getSimpleName();

    final String regex_notelp ="^(^\\+62\\s?|^0)(\\d{3,4}-?){2}\\d{3,4}$";
    final String regex_email ="(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])";

    TextInputEditText nama,email,alamat,detailalamat,notelp1,notelp2,sandi,konfirmasi;
    TextInputLayout nama_l,email_l,alamat_l,detailalamat_l,notelp1_l,notelp2_l,sandi_l,konfirmasi_l;
    ImageView profile_photo;
    TextView imagepath;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        nama = findViewById(R.id.pharmacy_name);
        email = findViewById(R.id.pharmacy_email);
        alamat = findViewById(R.id.address_pin);
        detailalamat = findViewById(R.id.pharmacy_address);
        notelp1 = findViewById(R.id.pharmacy_number);
        notelp2 = findViewById(R.id.pharmacy_scndnumber);
        sandi = findViewById(R.id.pharmacy_password);
        konfirmasi = findViewById(R.id.konfirmasi);

        imagepath = findViewById(R.id.imagepath);

        nama_l = findViewById(R.id.pharmacy_name_layout);
        email_l = findViewById(R.id.pharmacy_email_layout);
        alamat_l = findViewById(R.id.address_pin_layout);
        detailalamat_l = findViewById(R.id.pharmacy_address_layout);
        notelp1_l = findViewById(R.id.pharmacy_number_layout);
        notelp2_l = findViewById(R.id.pharmacy_scndnumber_layout);
        sandi_l = findViewById(R.id.pharmacy_password_layout);
        konfirmasi_l = findViewById(R.id.konfirmasi_layout);

        profile_photo = findViewById(R.id.pharmacy_logo);
    }

    public void PilihFoto(View view) {
        check_permission();
    }

    public void gotoPilihAlamatPin(View view) {
        ActivityCompat.requestPermissions(ActivityRegister.this,new String[]{ACCESS_FINE_LOCATION},RequestPermissionCode);
    }
    private void check_permission(){
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            // Permission is not granted
            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL);
            } else {
                // No explanation needed; request the permission
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL);

                // MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL is an
                // app-defined int constant. The callback method gets the
                // result of the request.
            }
        }else{
            // Permission has already been granted
            pickImagefromGallery();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == RequestPermissionCode) {// If request is cancelled, the result arrays are empty.
            if (grantResults.length > 0
                    && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                pilih_lokasi();
            } else {
                // Permission denied, Disable the functionality that depends on this permission.
                Toast.makeText(this, "Harap mengijinkan akses lokasi", Toast.LENGTH_LONG).show();
            }

            // other 'case' lines to check for other permissions this app might request.
            //You can add here other case statements according to your requirement.
        }else if (requestCode == MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL) {// If request is cancelled, the result arrays are empty.
            if (grantResults.length > 0
                    && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // permission was granted, yay! Do the
                // contacts-related task you need to do.
                pickImagefromGallery();
            } else {
                // permission denied, boo! Disable the
                // functionality that depends on this permission.
                Toast.makeText(this, "Akses storage ditolak, tidak dapat melakukan upload foto", Toast.LENGTH_SHORT).show();
            }

            // other 'case' lines to check for other
            // permissions this app might request.
        }
    }

    private void pilih_lokasi(){
        Intent intent=new Intent(ActivityRegister.this,ActivityPilihLokasi.class);
        startActivityForResult(intent, REQUEST_ADDRESS_CODE);
    }

    private void pickImagefromGallery(){
        CropImage.activity()
                .setAutoZoomEnabled(false)
                .setMaxCropResultSize(3000,3000)
                .setGuidelines(CropImageView.Guidelines.ON)
                .start(this);
    }

    public void registerpharmacy(View view) {
        if(validate()){
            registerPharmacy(path);
        }
    }

    private void registerPharmacy(String path) {
        final ProgressDialog pd = new ProgressDialog(this);
        pd.setMessage("Mohon Tunggu...");
        pd.setCancelable(false);
        pd.show();
        //creating a file
        File file = new File(path);
        //creating request body for file
        RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), file);
        MultipartBody.Part body = MultipartBody.Part.createFormData("image", file.getName(), requestFile);
//        RequestBody descBody = RequestBody.create(MediaType.parse("text/plain"), desc);
        JSONObject json = new JSONObject();
        try {
            json.put("pharmacy_name",nama.getText().toString());
            json.put("pharmacy_email",email.getText().toString());
            json.put("pharmacy_password",sandi.getText().toString());
            json.put("pharmacy_address",detailalamat.getText().toString());
            json.put("pharmacy_latitude",latitude);
            json.put("pharmacy_longitude",longitude);
            json.put("pharmacy_number",notelp1.getText().toString());
            json.put("pharmacy_scndnumber",notelp2.getText().toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        RequestBody jsonRequest = RequestBody.create(MediaType.parse("text/plain"), json.toString());
        Log.d("json", json.toString());

        Call<BaseResponse> call;
        API service = Client.getAPI();
        call = service.registerpharmacy(jsonRequest,body);

        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(@NonNull Call<BaseResponse> call, @NonNull
                    Response<BaseResponse> response) {
                if(response.isSuccessful()) {
                    BaseResponse result =response.body();
                    assert result != null;
                    if(!result.isError()){
                        finish();
                    }
                    Toast.makeText(ActivityRegister.this, result.getMessage(), Toast.LENGTH_SHORT).show();
                }else{
                    Toast.makeText(ActivityRegister.this, "Request Failed", Toast.LENGTH_SHORT).show();
                }
                pd.dismiss();
            }

            @Override
            public void onFailure(@NonNull Call<BaseResponse> call, @NonNull Throwable t) {
                t.printStackTrace();
                Toast.makeText(ActivityRegister.this, getString(R.string.koneksiTidakStabil),
                        Toast.LENGTH_SHORT).show();
                pd.dismiss();
            }
        });
    }

    private boolean validate() {
        String s_nama,s_email,s_alamat,s_detailalamat,s_notelp1,s_notelp2,s_sandi,s_konfirmasi;
        s_nama = nama.getText().toString();
        s_email = email.getText().toString();
        s_alamat = alamat.getText().toString();
        s_detailalamat = detailalamat.getText().toString();
        s_notelp1 = notelp1.getText().toString();
        s_notelp2 = notelp2.getText().toString();
        s_sandi = sandi.getText().toString();
        s_konfirmasi = konfirmasi.getText().toString();

        nama_l.setErrorEnabled(false);
        email_l.setErrorEnabled(false);
        alamat_l.setErrorEnabled(false);
        detailalamat_l.setErrorEnabled(false);
        notelp1_l.setErrorEnabled(false);
        notelp2_l.setErrorEnabled(false);
        sandi_l.setErrorEnabled(false);
        konfirmasi_l.setErrorEnabled(false);

        if(s_nama.isEmpty()){
            nama_l.setError("Nama Apotik harus diisi");
            return false;
        }
        if(s_email.isEmpty()){
            email_l.setError("Email Apotik harus diisi");
            return false;
        }
        if(!s_email.matches(regex_email)){
            email_l.setError("Format Email Tidak Valid");
            return false;
        }
        if(!imageselected){
            imagepath.setText("Harap memilih foto Apotik");
            imagepath.setTextColor(this.getResources().getColor(R.color.red_orange));
            return false;
        }
        if(s_alamat.isEmpty()){
            alamat_l.setError("Pilih alamat apotik sesuai titik");
            return false;
        }
        if(s_detailalamat.isEmpty()){
            detailalamat_l.setError("Alamat Apotik harus diisi");
            return false;
        }
        if(s_notelp1.isEmpty()){
            notelp1_l.setError("Nomor Telfon Apotik harus diisi");
            return false;
        }
        if(!s_notelp1.matches(regex_notelp)){
            notelp1_l.setError("Nomor Telfon tidak valid");
            return false;
        }
        if(!s_notelp2.matches(regex_notelp) && !s_notelp2.isEmpty()){
            notelp2_l.setError("Nomor Telfon tidak valid");
            return false;
        }
        if(s_sandi.isEmpty()){
            sandi_l.setError("Masukkan Sandi");
            return false;
        }
        if(s_sandi.length()<8){
            sandi_l.setError("Sandi harus terdiri dari minimal 8 karakter");
            return false;
        }
        if(!s_konfirmasi.equals(s_sandi)){
            konfirmasi_l.setError("Konfirmasi sandi tidak sesuai dengan sandi");
            return false;
        }
        return true;

    }
    boolean imageselected=false;
    String path;
    double latitude,longitude;
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        try {
            super.onActivityResult(requestCode, resultCode, data);

            if (requestCode == REQUEST_ADDRESS_CODE  && resultCode  == RESULT_OK) {

                String address = data.getStringExtra("address");
                alamat.setText(address);

                latitude = data.getDoubleExtra("latitude",0);
                longitude = data.getDoubleExtra("longitude",0);
            }

            if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
                // Get the Image from data
                CropImage.ActivityResult result = CropImage.getActivityResult(data);
                if (resultCode == RESULT_OK) {
                    Uri selectedImage = result.getUri();
                    String filename = selectedImage.getPath().substring(selectedImage.getPath().lastIndexOf("/")+1);
                    imagepath.setText("Image Selected");
                    imagepath.setTextColor(this.getResources().getColor(R.color.colorPrimary));
//                    String[] projection = {MediaStore.Images.Media.DATA};
//
//                    Cursor cursor = getContentResolver().query(selectedImage, projection, null, null, null);
//                    cursor.moveToFirst();
//
//                    int columnIndex = cursor.getColumnIndex(projection[0]);
//                    String picturePath = cursor.getString(columnIndex); // returns null
//                    cursor.close();
                    imageselected=true;
                    profile_photo.setImageURI(selectedImage);
                    path = selectedImage.getPath();
                    //tambah_foto(selected_id, selectedImage.getPath());
                } else {
                    Log.d("resultcode", String.valueOf(result.getError()));
                }
            }
        } catch (Exception ex) {
            Log.d(TAG, "onActivityResult: "+ex.toString());
        }
    }
}
