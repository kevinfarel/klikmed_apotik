package com.example.klikmed_apotik;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;

import com.ogaclejapan.smarttablayout.SmartTabLayout;
import com.ogaclejapan.smarttablayout.utils.v4.FragmentPagerItemAdapter;
import com.ogaclejapan.smarttablayout.utils.v4.FragmentPagerItems;

public class ActivityHome extends AppCompatActivity implements
        FragmentOrder.OnFragmentInteractionListener,
        FragmentDikirim.OnFragmentInteractionListener,
        FragmentSelesai.OnFragmentInteractionListener,
        FragmentTambahObat.OnFragmentInteractionListener{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        initComponent();
    }
    private void initComponent() {
        FragmentPagerItemAdapter adapter = new FragmentPagerItemAdapter(
                getSupportFragmentManager(), FragmentPagerItems.with(this)
                .add(R.string.title_order, FragmentOrder.class)
                .add(R.string.title_siap_dikirim, FragmentDikirim.class)
                .add(R.string.title_transaksi_selesai, FragmentSelesai.class)
                .add(R.string.title_tambah_obat, FragmentTambahObat.class)
                .create());

        ViewPager viewPager = findViewById(R.id.viewpager);
        viewPager.setAdapter(adapter);

        SmartTabLayout viewPagerTab = findViewById(R.id.viewpagertab);
        viewPagerTab.setViewPager(viewPager);
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }

    public void gotoProfile(View view) {
        Intent i = new Intent(this,ActivityProfile.class);
        startActivity(i);
    }

}
