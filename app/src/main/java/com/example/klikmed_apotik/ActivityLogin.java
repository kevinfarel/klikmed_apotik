package com.example.klikmed_apotik;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.example.klikmed_apotik.retrofit.API;
import com.example.klikmed_apotik.retrofit.Client;
import com.example.klikmed_apotik.retrofit.Response.LoginResponse;
import com.example.klikmed_apotik.session.SessionManager;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import org.json.JSONException;
import org.json.JSONObject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivityLogin extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        initComponent();
    }
    TextInputEditText email,sandi;
    TextInputLayout l_email,l_sandi;
    Toolbar toolbar;
    private void initComponent() {
        l_email = findViewById(R.id.notelp_layout);
        l_sandi = findViewById(R.id.sandi_layout);

        toolbar = findViewById(R.id.toolbar);
        email = findViewById(R.id.email);
        sandi = findViewById(R.id.sandi);

        toolbar.setNavigationOnClickListener(view -> ActivityLogin.super.onBackPressed());
    }

    private boolean validate(String email,String password) {
        l_email.setErrorEnabled(false);
        l_sandi.setErrorEnabled(false);
        if(email.isEmpty()){
            l_email.setError("Email harus diisi");
            return false;
        }else if(password.isEmpty()){
            l_sandi.setError("Password harus diisi");
            return false;
        }else{
            return true;
        }
    }


    public void Masuk(View view) {
        if(validate(email.getText().toString(),sandi.getText().toString())){
            doLogin(email.getText().toString(),sandi.getText().toString());
        }
    }

    private void doLogin(String email, String password) {
        ProgressDialog progressDialog = ProgressDialog.show(ActivityLogin.this, "",
                "Mohon Tunggu", true);
        progressDialog.show();

        JSONObject json = new JSONObject();

        try {
            json.put("email", email);
            json.put("password", password);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Log.d("json", json.toString());

        Call<LoginResponse> call;
        API service = Client.getAPI();
        call = service.login(json.toString());

        call.enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(@NonNull Call<LoginResponse> call, @NonNull
                    Response<LoginResponse> response) {
                if(response.isSuccessful()) {
                    LoginResponse result =response.body();
                    Toast.makeText(ActivityLogin.this, result.getMessage(), Toast.LENGTH_SHORT).show();
                    if(!result.isError()){
                        Intent i = new Intent(ActivityLogin.this,ActivityHome.class);
                        startActivity(i);
                        ActivityCompat.finishAffinity(ActivityLogin.this);
                        new SessionManager(ActivityLogin.this).saveAccountApotik(result.getPharmacy());
                    }
                }else{
                    Toast.makeText(ActivityLogin.this, "Request Failed", Toast.LENGTH_SHORT).show();
                }
                progressDialog.dismiss();
            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                t.printStackTrace();
                Toast.makeText(ActivityLogin.this, getString(R.string.koneksiTidakStabil),
                        Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
            }
        });
    }
}
