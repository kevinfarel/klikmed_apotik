package com.example.klikmed_apotik.Adapter;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;


import com.example.klikmed_apotik.R;
import com.example.klikmed_apotik.model.Medicine;

import java.util.ArrayList;

public class Adapter_pilihobat extends RecyclerView.Adapter<Adapter_pilihobat.holder> {
    Context context;
    ArrayList<Medicine> list;
    AdapterCallback adapterCallback;

    public Adapter_pilihobat(Context context, ArrayList<Medicine> list, AdapterCallback adapterCallback) {
        this.context = context;
        this.list = list;
        this.adapterCallback = adapterCallback;
    }

    @NonNull
    @Override
    public Adapter_pilihobat.holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
       LayoutInflater layoutInflater = LayoutInflater.from(context);
       View view = layoutInflater.inflate(R.layout.item_pilih_obat,parent,false);
       return new Adapter_pilihobat.holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull Adapter_pilihobat.holder holder, int position) {
        Medicine m = list.get(position);
        holder.nama.setText(m.getMedicine_name());
        holder.nama.setEllipsize(TextUtils.TruncateAt.MARQUEE);
        holder.nama.setSingleLine(true);
        holder.nama.setMarqueeRepeatLimit(5);
        holder.nama.setOnClickListener(view -> holder.nama.setSelected(true));
        holder.satuan.setText("Satuan : "+m.getMedicine_type());
        holder.pilih.setOnClickListener(view -> adapterCallback.onObatDipilih(m));
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public interface AdapterCallback {
        void onObatDipilih(Medicine m);
    }

    class holder extends RecyclerView.ViewHolder {
        TextView nama,pilih,satuan;
        holder(@NonNull View itemView) {
            super(itemView);
            nama = itemView.findViewById(R.id.nama_obat);
            pilih = itemView.findViewById(R.id.pilih_obat);
            satuan = itemView.findViewById(R.id.satuan);
        }
    }
}
