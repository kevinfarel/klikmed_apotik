package com.example.klikmed_apotik.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.klikmed_apotik.R;
import com.example.klikmed_apotik.model.PharmacyMedicine;

import java.util.ArrayList;

public class Adapter_obat extends RecyclerView.Adapter<Adapter_obat.ViewHolder> {
    private Context context;
    private ArrayList<PharmacyMedicine> medicines;
    private AdapterCallback adapterCallback;

    public Adapter_obat(Context context, ArrayList<PharmacyMedicine> medicines, AdapterCallback adapterCallback) {
        this.context = context;
        this.medicines = medicines;
        this.adapterCallback = adapterCallback;
    }

    @NonNull
    @Override
    public Adapter_obat.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        View view = layoutInflater.inflate(R.layout.item_medicines,parent,false);
        return new Adapter_obat.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull Adapter_obat.ViewHolder holder, int position) {
        PharmacyMedicine m = medicines.get(position);
        holder.medicine_name.setText(m.getMedicine_name());
        holder.medicine_price.setText("Rp. "+m.getMedicine_price());
        holder.stock.setText("Stock : "+m.getMedicine_qty());
        holder.delete.setOnClickListener(view -> adapterCallback.onDelete(m.getPharmacy_medicine_id(),m.getMedicine_name()));
        holder.edit.setOnClickListener(view -> adapterCallback.onEdit(view,m.getPharmacy_medicine_id(),m.getMedicine_qty(),m.getMedicine_name(),m.getMedicine_type(),m.getMedicine_price()));
    }

    @Override
    public int getItemCount() {
        return medicines.size();
    }

    public interface AdapterCallback{
        void onDelete(int pharmacy_medicine_id,String medicine_name);
        void onEdit (View view,int pharmacy_medicine_id,int stock, String medicine_name, String medicine_type, int medicine_price);
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        TextView medicine_name,medicine_price,stock;
        ImageView edit,delete;
        ViewHolder(@NonNull View itemView) {
            super(itemView);
            medicine_name = itemView.findViewById(R.id.medicine_name);
            medicine_price = itemView.findViewById(R.id.medicine_price);
            stock = itemView.findViewById(R.id.stock);
            edit = itemView.findViewById(R.id.edit);
            delete = itemView.findViewById(R.id.delete);
        }
    }
}
