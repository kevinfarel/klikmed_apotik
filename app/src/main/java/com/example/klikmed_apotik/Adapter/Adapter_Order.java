package com.example.klikmed_apotik.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.klikmed_apotik.R;
import com.example.klikmed_apotik.model.Order;
import com.example.klikmed_apotik.model.Recipe;

import java.util.ArrayList;

public class Adapter_Order extends RecyclerView.Adapter<Adapter_Order.ViewHolder>{
    private Context context;
    private ArrayList<Order> orders;
    private AdapterCallback adapterCallback;

    public Adapter_Order(Context context, ArrayList<Order> orders, AdapterCallback adapterCallback) {
        this.context = context;
        this.orders = orders;
        this.adapterCallback = adapterCallback;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        View view = layoutInflater.inflate(R.layout.item_order,parent,false);
        return new Adapter_Order.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Order order = orders.get(position);
        holder.nama.setText(order.getPatient().getPatient_name());
        holder.telfon.setText(order.getPatient().getPatient_number());
        holder.telfon.setOnClickListener(view -> adapterCallback.TelfonCallback(order.getPatient().getPatient_number()));
        holder.timestamp.setText(order.getCreated_at());

        StringBuilder listobat = new StringBuilder();
        for (Recipe recipe:order.getRecipes()) {
            listobat.append(recipe.getMedicine_name()).append("   X ").append(recipe.getRecipe_qty()).append('\n');
        }
        holder.listobat.setText(listobat.toString());

        holder.tolak.setOnClickListener(view -> adapterCallback.TolakCallback(order.getTransaction_id()));

        holder.terima.setOnClickListener(view -> adapterCallback.TerimaCallback(order.getTransaction_id()));
    }



    @Override
    public int getItemCount() {
        return orders.size();
    }

    public interface AdapterCallback {
        void TolakCallback(int transaction_id);
        void TerimaCallback(int transaction_id);
        void TelfonCallback(String phonenumber);
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        TextView nama,telfon,timestamp,listobat,tolak,terima;
        ViewHolder(@NonNull View itemView) {
            super(itemView);
            nama = itemView.findViewById(R.id.nama_pembeli);
            telfon = itemView.findViewById(R.id.telfon_pembeli);
            timestamp = itemView.findViewById(R.id.timestamp);
            listobat = itemView.findViewById(R.id.list_obat);
            tolak = itemView.findViewById(R.id.tolak);
            terima = itemView.findViewById(R.id.terima);
        }
    }
}
