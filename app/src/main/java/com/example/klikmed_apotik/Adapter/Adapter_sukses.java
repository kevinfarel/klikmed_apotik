package com.example.klikmed_apotik.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.klikmed_apotik.R;
import com.example.klikmed_apotik.model.FinishOrder;
import com.example.klikmed_apotik.model.Medicine;
import com.example.klikmed_apotik.model.MedicineOrders;
import com.example.klikmed_apotik.model.SuccessOrder;

import java.util.ArrayList;

public class Adapter_sukses extends RecyclerView.Adapter<Adapter_sukses.ViewHolder> {
    Context context;
    ArrayList<SuccessOrder> successOrders;
    AdapterCallback adapterCallback;

    public Adapter_sukses(Context context, ArrayList<SuccessOrder> successOrders, AdapterCallback adapterCallback) {
        this.context = context;
        this.successOrders = successOrders;
        this.adapterCallback = adapterCallback;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        View view = layoutInflater.inflate(R.layout.item_ordersukses,parent,false);
        return new Adapter_sukses.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        SuccessOrder successOrder = successOrders.get(position);
        holder.nama.setText(successOrder.getPatient().getPatient_name());
        holder.telfon.setText(successOrder.getPatient().getPatient_number());
        holder.telfon.setOnClickListener(view -> adapterCallback.TelfonCallback(successOrder.getPatient().getPatient_number()));
        holder.timestamp.setText(successOrder.getUpdated_at());

        StringBuilder listobat = new StringBuilder();
        for (MedicineOrders medicineOrders:successOrder.getMedicines()) {
            listobat.append(medicineOrders.getMedicine_name()).append("   X ").append(medicineOrders.getMedicine_qty()).append('\n');
        }
        holder.listobat.setText(listobat);
        holder.total_price.setText("Rp "+successOrder.getTotal_price());

    }

    @Override
    public int getItemCount() {
        return successOrders.size();
    }

    public interface AdapterCallback{
        void TelfonCallback(String number);
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        TextView nama,telfon,timestamp,listobat,status,total_price;
        ViewHolder(@NonNull View itemView) {
            super(itemView);
            nama = itemView.findViewById(R.id.nama_pembeli);
            telfon = itemView.findViewById(R.id.telfon_pembeli);
            timestamp = itemView.findViewById(R.id.timestamp);
            listobat = itemView.findViewById(R.id.list_obat);
            status = itemView.findViewById(R.id.status_order);
            total_price = itemView.findViewById(R.id.total_price);
        }
    }
}
