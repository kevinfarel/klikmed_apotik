package com.example.klikmed_apotik.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.example.klikmed_apotik.R;
import com.example.klikmed_apotik.model.FinishOrder;
import com.example.klikmed_apotik.model.Medicine;
import com.example.klikmed_apotik.model.MedicineOrders;
import com.example.klikmed_apotik.model.Order;

import java.util.ArrayList;

public class Adapter_siapdikirim extends RecyclerView.Adapter<Adapter_siapdikirim.ViewHolder> {
    Context context;
    ArrayList<FinishOrder> finishOrders;
    AdapterCallback adapterCallback;

    public Adapter_siapdikirim(Context context, ArrayList<FinishOrder> finishOrders, AdapterCallback adapterCallback) {
        this.context = context;
        this.finishOrders = finishOrders;
        this.adapterCallback = adapterCallback;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        View view = layoutInflater.inflate(R.layout.item_siapdikirim,parent,false);
        return new Adapter_siapdikirim.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        FinishOrder finishOrder = finishOrders.get(position);
        holder.nama.setText(finishOrder.getPatient().getPatient_name());
        holder.telfon.setText(finishOrder.getPatient().getPatient_number());
        holder.telfon.setOnClickListener(view -> adapterCallback.TelfonCallback(finishOrder.getPatient().getPatient_number()));
        holder.timestamp.setText(finishOrder.getCreated_at());
        holder.alamat.setText(finishOrder.getPatient().getAlamat_detail());

        StringBuilder listobat = new StringBuilder();
        for (MedicineOrders medicineOrders:finishOrder.getMedicines()) {
            listobat.append(medicineOrders.getMedicine_name()).append("   X ").append(medicineOrders.getMedicine_qty()).append('\n');
        }
        holder.listobat.setText(listobat.toString());
        if(finishOrder.getTransaction_status()==1){
            holder.status.setText("Menunggu Konfirmasi Pembayaran");
        }else if(finishOrder.getTransaction_status()==2){
            holder.status.setText("Pesanan Siap Dikirim");
            if(finishOrder.getCourier_info()==null) {
                holder.sendcourier_info.setVisibility(View.VISIBLE);
                holder.sendcourier_info.setOnClickListener(view -> adapterCallback.onPressedInfoCourier(finishOrder.getTransaction_id()));
            }
        }
    }

    @Override
    public int getItemCount() {
        return finishOrders.size();
    }

    public interface AdapterCallback{
        void TelfonCallback(String number);
        void onPressedInfoCourier(int transaction_id);
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        TextView nama,telfon,timestamp,listobat,status,sendcourier_info,alamat;
        ViewHolder(@NonNull View itemView) {
            super(itemView);
            nama = itemView.findViewById(R.id.nama_pembeli);
            telfon = itemView.findViewById(R.id.telfon_pembeli);
            timestamp = itemView.findViewById(R.id.timestamp);
            listobat = itemView.findViewById(R.id.list_obat);
            status = itemView.findViewById(R.id.status_order);
            sendcourier_info = itemView.findViewById(R.id.info_pengirim_input);
            alamat = itemView.findViewById(R.id.alamat);
        }
    }
}
